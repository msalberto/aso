#!/bin/bash

#######################################
#####  SFS (SCRIPT FIND SCRIPTS)  #####
## Script que busca en el directorio ##
## pasado como primer argumento todos##
## los scripts, siendo script todo   ##
## fichero que comience por: 		 ##
## #!/bin/bash                       ##
#######################################

#Indicamos el numero de parametros que se esperan
EXPECTED_ARGS=1

#Codigo de error en numero de argumentos
E_BADARGS=65

#Codigo de error en la ruta
E_BADPATH=66

#Contador asociado al numero de scripts que se encuentran
let contador=0

echo "Iniciando busqueda de scripts."

#Comprobamos si el numero de argumentos es incorrecto
if [ $# -ne $EXPECTED_ARGS ]
then
  	echo "Error: Falta la ruta de entrada" >&2
  	#Retorna el codigo de error de numero de argumentos erroneo
 	exit $E_BADARGS
fi

#Comprobamos si la ruta existe
if [ ! -d "${1}" ]
then
	echo "Error: La ruta no existe" >&2
  	#Retorna el codigo de error de numero de argumentos erroneo
  	exit $E_BADPATH
fi

echo "Buscando en el directorio: ${1}"

#Iteramos con "find" para encontrar todos los archivos
for f in $(find "${1}" -type f)
do 
	#Obtenemos la primera línea del fichero
	line=$(head -n 1 "${f}")
	#La comparamos para saber si es un script
	if [ "${line}" == "#!/bin/bash" ]
	then
		$((contador++))
		echo "${contador}: ${f}"
	fi
#Redirigimos la salida de error para evitar mostrar
#errores asociados a la falta de permisos para ver archivos
done 2>/dev/null

echo "${contador} scripts encontrados."
