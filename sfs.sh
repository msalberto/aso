#!/bin/bash

#######################################
#####  SFS (SCRIPT FIND SCRIPTS)  #####
## Script que busca en el directorio ##
## pasado como primer argumento todos##
## los scripts, siendo script todo   ##
## fichero que comience por:         ##
## #!/bin/bash u otro definido en    ##
## /etc/shells                       ##
#######################################

#Indicamos el numero de parametros que se esperan
EXPECTED_ARGS=1

#Variable global para opcion usuario
OPT_USER=0

#Variable global para opcion SETUID
OPT_UID=0

#Variable global para opcion SETGID
OPT_GID=0

#Codigo de salida por defecto
E_DEFAULT=0

#Codigo de error en numero de argumentos
E_BADARGS=65

#Codigo de error en la ruta
E_BADPATH=66

#Codigo de error fichero de shells
E_BADFILE=67

#Codigo de error en opcion
E_BADOPT=68

#Codigo de error en argumento de la opcion
E_BADOPTARG=69

#Codigo de error de fichero de ayuda no encontrado
E_BADHELP=70

#Fichero por defecto que indica los shells
FICH_SHELLS="/etc/shells"

#Fichero por defecto de ayuda help
FICH_HELP="help.txt"

#Texto de ayuda, invocado con --help o -h
HELP_MSG="\n
Usage: ./sfs.sh [OPTIONS] [PATH] \n
List all the scripts found in PATH and its inner folders. \n
By default, SFS uses the /etc/shells file to check if a file is a script. \n
\n
List of possible OPTIONS: \n
\n
  -f [FILE] \t   specify a file with a list of possible shells \n
   \t\t          whose scripts will be identified (substitutes /etc/shells)\n
  -g \t\t        show only those scripts whose SETGID bit is activated \n
  -h, --help \t  show this help \n
  -i \t\t        show only those scripts whose SETUID bit is activated \n
  -u \t\t        show only those scripts that have execution permission \n
   \t\t          for the current user \n
\n
Exit status: \n
  0  \t if OK, \n
  65 \t if bad number of arguments, \n
  66 \t if incorrect or inexisting path, \n
  67 \t if FILE does not exist, \n
  68 \t if incorrect option, \n
  69 \t if incorrect argument in option, \n
  70 \t if help file does not exist. \n
\n
For complete documentation, run: man sfs (when implemented). \n"

#Contador asociado al numero de scripts que se encuentran
let contador=0 #Variable
let indice=0   #Variable



#Comprobamos si el numero de argumentos es incorrecto
if [ $# -lt $EXPECTED_ARGS ]
then
        echo "Error: Falta la ruta de entrada" >&2
        #Retorna el codigo de error de numero de argumentos erroneo
        exit $E_BADARGS
fi


# Con getopts estudiamos que los argumentos sean los correctos
 

while getopts ":f: :u :i :g" opt
do
case $opt in
    f)
        FICH_SHELLS="$(echo ${OPTARG})"
    ;;

    u)
        OPT_USER="$(echo 1)"
    ;;

    i)
        OPT_UID="$(echo 1)"
    ;;

    g)
        OPT_GID="$(echo 1)"
    ;;

    \?)
       # Para el caso de "--help" al ser un parametro largo
       # hay que estudiar la ristra entera.
        while test $# -gt 0
        do
            case $1 in

  
                 -h | --help)
                    #if [ ! -f "${FICH_HELP}" ]
                    #then
                    #echo "Error: no se encuentra fichero de ${FICH_HELP} "  
                    #exit $E_BADHELP
                    #fi
                    # Mostramos por consola la pequeña ayuda y salimos de la ejecucion
                    #cat $FICH_HELP
                    echo -e ${HELP_MSG}
                    exit $E_DEFAULT
                ;;
    
                \?)
                    echo "Opcion no valida: -$OPTARG" >&2
                    exit $E_BADOPT
                ;;
    
            esac
  
        done
       
    ;;

    :)
        echo "La opcion -$OPTARG requiere un argumento." >&2
        exit $E_BADOPTARG
    ;;
esac
done

echo "Iniciando busqueda de scripts."
#Comprobamos si la ruta existe (es el ultimo parametro)
if [ ! -d "${!#}" ] #Referencia indirecta a la variable $# - Permite definir variables cuyo contenido es el nombre de otra variable
then
    echo "Error: La ruta no existe" >&2
    #Retorna el codigo de error de numero de argumentos erroneo
    exit $E_BADPATH
fi

#Comprobamos si el fichero de shells existe
if [ ! -f "${FICH_SHELLS}" ]
then
    echo "Error: El fichero de shells no existe" >&2
    #Retorna el codigo de error de numero de argumentos erroneo
    exit $E_BADFILE
fi

echo "Buscando en el directorio: ${!#}"
#Leer 

#Ejecutamos la lectura aparte para que los cambios dentro del while se mantengan
exec 3< <(cat ${FICH_SHELLS})
while read LINE
do
        if [[ $(echo ${LINE}|cut -c1-1) = "/" ]]
    then
        SHELLS[${indice}]="$(echo "#!${LINE}")"             
        ((indice++))
    fi
done <&3
exec 3<&-

#Decrementamos para tener el numero de shells
((indice--))
#Prueba comentario


#Iteramos con "find" para encontrar todos los archivos
for f in $(find "${!#}" -type f)
do
        #Obtenemos la primera línea del fichero
        linea=$(head -n 1 "${f}")
        #La comparamos con los bash en el array
        #para saber si es un script
        for i in ${SHELLS[@]}
        do
                if [[ ${i} == "${linea}" ]]
                then
            #Hacemos las comprobaciones de las opciones

            #Permisos de ejecucion para el usuario
            if [[ $OPT_USER == 1 && ! -x "${f}" ]] 
            then
                break
            fi

            #Activado el bit SETUID   
            if [[ $OPT_UID == 1 && ! -u "${f}" ]]
            then
                break
            fi

            #Activado el bit SETGID 
            if [[ $OPT_GID == 1 && ! -g "${f}" ]]
            then
                break
            fi

            #Si hemos llegado hasta aqui, se cumplen las opciones
                        $((contador++))
                        echo "${contador}: ${f}"
                fi
        done
        
#Redirigimos la salida de error para evitar mostrar
#errores asociados a la falta de permisos para ver archivos
done 2>/dev/null

echo "${contador} scripts encontrados."
exit $E_DEFAULT

